/*
 * Einstiegspunkt für diese App steht ganz am Ende dieser JS-Datei. 
 * 
 * Start des Scripts: 
 * 
 *      node fruehstueck.js
 */

function check(bedingung, nachricht) {
    if(bedingung) {
        console.log(nachricht);
        return false;
    }
    return true;
}

function istFertig(teeTasse, marmeladenToast, kaeseToast, hartGekochtesEi, apfelStuecke) {

    check(teeTasse.wasser == null, "Kein Wasser in Tasse");
    check(teeTasse.wasser.menge < 250, "Tasse ist nicht voll");
    check(teeTasse.teeGezogen == false, "Da war kein Teebeutel im Wasser");
    check(teeTasse.milch == null, "Ich möchte den Tee mit Milch, bitte");
    check(teeTasse.zucker == null, "Ich möchte den Tee mit Zucker, bitte");
    check(teeTasse.umgeruehrt == false, "Bitte den Tee mit Milch und Zucker umrühren");
    console.log('Tee OK');

    check(marmeladenToast.getoastet == false, "Mein Marmeladenbrot ist gar nicht knusprig!");
    check(marmeladenToast.butter == null, "Ich möchte bitte Butter auf mein Marmeladenbrot!");
    check(marmeladenToast.marmelade == null, "Da fehlt die Marmelade.");
    check(marmeladenToast.kaese != null, "Käse auf dem Marmeladenbrot? Eigentlich lecker, aber hier zu gewagt!");
    console.log('Marmeladentoast OK');

    check(kaeseToast.getoastet == false, "Mein Käsebrot ist gar nicht knusprig!");
    check(kaeseToast.butter == null, "Ich möchte bitte Butter auf mein Käsebrot!");
    check(kaeseToast.kaese == null, "Da fehlt der Käse.");
    check(kaeseToast.butter == marmeladenToast.butter, "Du Sparfuchs! Zwei verschiedene Butterstücke, bitte!");
    check(kaeseToast.marmelade != null, "Marmelade auf dem Käsebrot? Eigentlich lecker, aber hier zu gewagt!");
    console.log('Käsetoast OK');

    check(hartGekochtesEi.hartgekocht == false, "Das Ei ist nicht hartgekocht.");
    console.log('Ei OK');

    check(apfelStuecke.length != 6, "Ich hätte gerne genau 6 Apfelstücke");
    apfelStuecke.forEach(
        function(apfelStueck, index, apfelStuecke) {
            check(apfelStueck == null, "Da fehlt ein Apfelstück");
            check(apfelStueck.gewaschen == false, "Das Apfelstück ist nicht gewaschen.");
        }
    );
    console.log('Apfelstücke OK');

}


/* 
 * Hilfsmethode zum Warten für eine Dauer in Millisekunden 
 */
function sleep(milliseconds) {
    let start = new Date().getTime();
    for (let i = 0; i < 1e7; i++) {
        if ((new Date().getTime() - start) > milliseconds) {
            break;
        }
    }
}

/* 
 * Das Wasser hat eine Menge in Milliliter 
 * und eine Temparatur in Grad Celsius.
 */
class Wasser {
    constructor(menge, temperatur) {
        this.menge = menge;
        this.temperatur = temperatur;
    }

    entnehmen(menge) {
        if(this.menge >= menge) {
            this.menge -= menge;
        } else {
            console.log('Wassermenge zu gering für die Entnahme.')
        }
    }
}

/*
 * Basis für alle Küchenutensilien
 */
class Utensil {
    constructor(name) {
        this.name = name;
        this.inBenutzung = false;
    }

    // Dauer in Sekunden warten
    benutzen(sekunden) {
        this.inBenutzung = true;
        console.log('Benutze ' + this.name + ' für ' + sekunden + ' Sekunden.');
        sleep(sekunden * 1000);
        this.inBenutzung = false;
    }
}

class WasserBehaelter extends Utensil {
    constructor(name, maxFuellMenge) {
        super(name);
        this.maxFuellMenge = maxFuellMenge;
        this.wasser = null;
    }

    einschenken(wasser) {
        let einzuschaenkendeMenge = Math.min(wasser.menge, this.maxFuellMenge);
        wasser.entnehmen(einzuschaenkendeMenge);
        this.wasser = new Wasser(einzuschaenkendeMenge, wasser.temperatur);
    }
}

class Tasse extends WasserBehaelter {
    constructor() {
        super('Tasse', 250);

        this.teeBeutel = null;
        this.milch = null;
        this.zucker = null;

        this.umgeruehrt = false;
        this.teeGezogen = false;
    }

    milchEinschenken(milch) {
        this.milch = milch;
    }

    teeZiehenLassen(teeBeutel) {
        this.teeBeutel = teeBeutel

        if(this.wasser == null) {
            console.log('Kein Wasser in der Tasse.');
        }
        if(this.wasser.temperatur < 100) {
            console.log('Das Wasser kocht noch.');
        }

        this.benutzen(4 * 60); // 4 Minuten
        this.teeGezogen = true;
        this.umgeruehrt = false;
    }

    wasserEinschenken(wasser) {
        this.einschenken(wasser);
        this.umgeruehrt = false;
    }
}

class Loeffel extends Utensil {
    constructor() {
        super('Löffel');
    }

    umruehren(tasse) {
        this.benutzen(15);
        tasse.umgeruehrt = true;
    }

    zuckerLoeffeln(zucker, tasse) {
        this.benutzen(15);
        tasse.zucker = zucker;
        tasse.umgeruehrt = false;
    }

    marmeladeLoeffeln(marmelade, brotScheibe) {
        this.benutzen(15);
        brotScheibe.marmelade = marmelade;
    }
}

class Messer extends Utensil {
    constructor() {
        super('Messer');
    }

    schneiden(apfel) {
        this.benutzen(2 * 60);

        return [ 
            new ApfelStueck(apfel.gewaschen), new ApfelStueck(apfel.gewaschen),
            new ApfelStueck(apfel.gewaschen), new ApfelStueck(apfel.gewaschen),
            new ApfelStueck(apfel.gewaschen), new ApfelStueck(apfel.gewaschen),
        ]; 
    }

    schmieren(butter, brotScheibe) {
        if(brotScheibe.kaese != null) {
            console.log('Die Butter muss vor dem Käse auf das Brot.');
        }
        if(brotScheibe.marmelade != null) {
            console.log('Die Butter muss vor der Marmelade auf das Brot.')
        }

        this.benutzen(30);
        brotScheibe.butter = butter;
    }
}

class Toaster extends Utensil {
    constructor() {
        super('Toaster');
    }

    toasten(brotScheibe) {
        if(brotScheibe.marmelade != null) {
            console.log('Marmelade im Toaster - was für eine Sauerei.');
        }
        if(brotScheibe.kaese != null) {
            console.log('Käse im Toaster - was für eine Sauerei.')
        }
        if(brotScheibe.butter != null) {
            console.log('Butter im Toaster - was für eine Sauerei.')
        }

        this.benutzen(15);
        brotScheibe.getoastet = true;
    }
}

class Topf extends WasserBehaelter {
    constructor() {
        super('Topf', 500);
    }

    wasserKochen() {
        if(this.wasser == null) {
            console.log('Da ist gar kein Wasser im Topf.');
        }

        let menge = this.wasser.menge;
        this.benutzen((menge * 10 * 90) / 1000);
        this.wasser.temperatur = 100;
    }

    eiKochen(ei) {
        if(this.wasser == null) {
            console.log('Kein Wasser im Topf.');
        }
        if(this.wasser.temperatur < 100) {
            console.log('Wasser im Topf kocht noch nicht.');
        }
        if(ei.hartgekocht) {
            console.log('Das Ei ist bereits gekocht.');
        }

        this.benutzen(7 * 60);
        ei.hartgekocht = true;
    }
}

class WasserKocher extends WasserBehaelter {
    constructor() {
        super('Wasserkocher', 2000);
    }

    // Erhitzt Wasser auf 100°C. Dies dauert 1 Minute pro 200 mL Wasser
    kochen() {
        let menge = this.wasser.menge;
        this.benutzen(menge * 5 * 60 / 1000);
        this.wasser.temperatur = 100;
    }

    // Lässt das Wasser ab. Dies dauert 1 Sekunde pro 50 mL.
    wasserAblassen() {
        this.benutzen(this.wasser.menge * 20 / 1000);
        let wasser = this.wasser;
        this.wasser = null;
        
        return wasser;
    }

}

class WasserHahn extends Utensil {
    constructor() {
        super('Wasserhahn');
    }
    
    // Lasse Wasser aus dem Wasserhahn. Dies dauert 1 Sekunde pro 50 mL. 
    // Das Wasser hat stets 10°C.
    wasserEntnehmen(menge) {
        this.benutzen(menge * 20 / 1000);
        return new Wasser(menge, 10);
    }

    apfelWaschen(apfel) {
        this.benutzen(30);
        apfel.gewaschen = true;
    }
}


class Apfel {
    constructor() {
        this.gewaschen = false;
    }

    waschen() {
        this.gewaschen = true;
    }
}

class ApfelStueck {
    constructor(gewaschen) {
        this.gewaschen = gewaschen;
    }
}

class Butter {
}

class Marmelade {
}

class Kaese {

}

class BrotScheibe {
    constructor() {
        this.getoastet = false;

        this.butter = null;
        this.marmelade = null;
        this.kaese = null;
    }

    belegen(kaese) {
        this.kaese = kaese;
    }
}

class Ei {
    constructor() {
        this.hartGekocht = false;
    }
}

class Milch {
}

class TeeBeutel {
}

class Zucker {
}

function zubereiten() {
    console.log('Frühstück zubereiten ...');
    const startDerZubereitung = new Date();
    console.log(startDerZubereitung);

    let loeffel = new Loeffel();
    let tasse = new Tasse();
    let wasserHahn = new WasserHahn();
    let wasserKocher = new WasserKocher();
    let messer = new Messer();
    let toaster = new Toaster();
    let topf = new Topf();

    let maxFuellMenge = tasse.maxFuellMenge;
    let wasser = wasserHahn.wasserEntnehmen(maxFuellMenge);
    wasserKocher.einschenken(wasser);
    wasserKocher.kochen();
    tasse.wasserEinschenken(wasserKocher.wasserAblassen());
    tasse.milchEinschenken(new Milch());
    tasse.teeZiehenLassen(new TeeBeutel());
    loeffel.zuckerLoeffeln(new Zucker(), tasse);
    loeffel.umruehren(tasse);
    console.log('=> Der Tee ist fertig.');

    let marmeladenToast = new BrotScheibe();
    toaster.toasten(marmeladenToast);
    messer.schmieren(new Butter(), marmeladenToast);
    loeffel.marmeladeLoeffeln(new Marmelade(), marmeladenToast);
    console.log('=> Das Marmeladentoast ist fertig.');

    let kaeseToast = new BrotScheibe();
    toaster.toasten(kaeseToast);
    messer.schmieren(new Butter(), kaeseToast);
    kaeseToast.belegen(new Kaese());
    console.log('=> Das Käsetoast ist fertig.');

    let ei = new Ei();
    topf.einschenken(wasserHahn.wasserEntnehmen(topf.maxFuellMenge));
    topf.wasserKochen();
    topf.eiKochen(ei);
    console.log('=> Das Ei ist fertig.');

    let apfel = new Apfel();
    wasserHahn.apfelWaschen(apfel);
    let apfelStuecke = messer.schneiden(apfel);
    console.log('=> Der Apfel ist fertig.');

    istFertig(tasse, marmeladenToast, kaeseToast, ei, apfelStuecke);

    let dauer = (new Date() - startDerZubereitung) / 1000; 
    console.log('Das Frühstück ist nach ' + dauer + ' Minuten fertig');

}

// Einstiegspunkt der Frühstückszubereitung
zubereiten();

